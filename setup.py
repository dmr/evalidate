from setuptools import setup

setup(name='evalidate',
      version='0.6',
      description='Eval code validator',
      url='http://evalidate.readthedocs.org/',
      author='Yaroslav Polyakov',
      author_email='xenon@sysattack.com',
      license='MIT',
      packages=['evalidate'],
      zip_safe=False)

